import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:yebelo_project/model/product.model.dart';
import 'package:yebelo_project/screens/cart.screen.dart';
import 'package:yebelo_project/screens/product.screen.dart';
import 'package:yebelo_project/services/json.service.dart';
import 'package:yebelo_project/services/sharedpreferences.service.dart';

import 'navigation_drawer.dart';

void main() => runApp(const Home());

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List _items = [];
  String cartItemCount = '0';

  @override
  void initState() {
    super.initState();
    getLocalJsonData().then((data) {
      setState(() {
        _items = data;
      });
    });
    getCartData().then((count) {
      setState(() {
        cartItemCount = count.toString();
      });
    }).onError((error, stackTrace) {
      Fluttertoast.showToast(msg: error.toString());
    });
  }

  getDataByCategory(category) {
    if (category == 'All Categories') {
      getLocalJsonData().then((data) {
        setState(() {
          _items = data;
        });
      });
    } else {
      List newList = [];
      getLocalJsonData().then((data) {
        _items = data;
        for (var item in _items) {
          if (category == item['p_category']) {
            newList.add(item);
          }
        }
        setState(() {
          _items = newList;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: Colors.amber,
      home: Scaffold(
          drawer: const NavigationDrawer(),
          appBar: AppBar(
            title: const Text(
              'Home',
            ),
            actions: [
              PopupMenuButton(
                  icon: const Icon(Icons.sort),
                  onSelected: (item) {
                    getDataByCategory(item);
                    // setState(() {});
                  },
                  itemBuilder: (context) => [
                        const PopupMenuItem(
                          value: 'All Categories',
                          child: Text("All Categories"),
                        ),
                        const PopupMenuItem(
                          value: 'Premium',
                          child: Text("Premium"),
                        ),
                        const PopupMenuItem(
                          value: 'Tamilnadu',
                          child: Text("Tamilnadu"),
                        ),
                      ])
            ],
          ),
          body: Padding(
            padding: const EdgeInsets.all(5),
            child: Column(
              children: [
                _items.isNotEmpty
                    ? Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: _items.length,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                ProductModel product = ProductModel();
                                product.id = _items[index]["p_id"];
                                product.name = _items[index]["p_name"];
                                product.cost = _items[index]["p_cost"];
                                product.availability =
                                    _items[index]["p_availability"];
                                product.details = _items[index]["p_details"];
                                product.category = _items[index]["p_category"];
                                product.image = _items[index]["p_image"];
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Product(
                                              product: product,
                                            )));
                              },
                              child: Card(
                                margin: const EdgeInsets.all(5),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Column(
                                  children: [
                                    ClipRRect(
                                      borderRadius: const BorderRadius.vertical(
                                          top: Radius.circular(10.0)),
                                      child: Image.network(
                                          _items[index]["p_image"]),
                                    ),
                                    ListTile(
                                      title: Text(_items[index]["p_name"]),
                                      subtitle: Text((_items[index]
                                              ["p_details"] ??
                                          'No data')),
                                      trailing: Text(
                                        'Rs.${_items[index]["p_cost"]}',
                                        style: const TextStyle(fontSize: 15),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      )
                    : Container()
              ],
            ),
          ),
          floatingActionButton: FloatingActionButton(
            child: Badge(
              toAnimate: true,
              shape: BadgeShape.circle,
              badgeColor: Colors.red,
              borderRadius: BorderRadius.circular(8),
              badgeContent: Text(cartItemCount,
                  style: const TextStyle(color: Colors.white)),
              child: const Icon(
                Icons.shopping_cart,
              ),
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const Cart()));
            },
          )),
    );
  }
}
