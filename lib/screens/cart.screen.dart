import 'package:flutter/material.dart';
import 'package:yebelo_project/navigation_drawer.dart';
import 'package:yebelo_project/services/json.service.dart';
import 'package:yebelo_project/services/sharedpreferences.service.dart';

class Cart extends StatefulWidget {
  const Cart({Key? key}) : super(key: key);

  @override
  State<Cart> createState() => _CartState();
}

class _CartState extends State<Cart> {
  String itemName = '-';
  int itemQuantity = 0;
  int itemCost = 0;

  @override
  void initState() {
    super.initState();
    getCartItemData().then((data) {
      getItemJsonData(data).then((item) {
        setState(() {
          itemName = item['p_name'];
          itemQuantity = 1;
          itemCost = item['p_cost'];
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        drawer: const NavigationDrawer(),
        appBar: AppBar(
          title: const Text('Cart'),
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                DataTable(columns: const [
                  DataColumn(
                    label: Text('Name'),
                  ),
                  DataColumn(
                    label: Text('Quantity'),
                  ),
                  DataColumn(
                    label: Text('Amount'),
                  ),
                ], rows: [
                  DataRow(cells: [
                    DataCell(Text(itemName)),
                    DataCell(Text(itemQuantity.toString())),
                    DataCell(Text(itemCost.toString())),
                  ]),
                ]),
                ElevatedButton(onPressed: () {}, child: const Text('CheckOut')),
                ElevatedButton(
                    onPressed: () {
                      setState(() {
                        itemName = '-';
                        itemQuantity = 0;
                        itemCost = 0;
                      });
                      clearCart();
                    },
                    child: const Text('Clear Cart')),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
