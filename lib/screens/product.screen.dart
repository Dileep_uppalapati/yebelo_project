import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:yebelo_project/model/product.model.dart';
import 'package:yebelo_project/navigation_drawer.dart';
import 'package:yebelo_project/screens/cart.screen.dart';
import 'package:yebelo_project/services/sharedpreferences.service.dart';

// ignore: must_be_immutable
class Product extends StatelessWidget {
  ProductModel product = ProductModel();
  Product({Key? key, required this.product}) : super(key: key);
  String itemcount = '0';
  @override
  Widget build(BuildContext context) {
    getCartData().then((value) {
      itemcount = value.toString();
    });
    return MaterialApp(
      home: Scaffold(
        drawer: const NavigationDrawer(),
        appBar: AppBar(
          title: Text(product.name ?? 'Product Page'),
        ),
        body: Center(
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              children: [
                ClipRRect(
                  borderRadius:
                      const BorderRadius.vertical(top: Radius.circular(10.0)),
                  child: Image.network(product.image.toString()),
                ),
                ListTile(
                  title: Text(product.name.toString()),
                  subtitle: Text((product.details.toString())),
                  trailing: Text(
                    'Rs.${product.cost}',
                    style: const TextStyle(fontSize: 15),
                  ),
                ),
                Text('Available Quantity: ${product.availability}'),
                ElevatedButton(
                    onPressed: () {
                      addCartItem(product.id!);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Cart()));
                    },
                    child: const Text('Buy Now')),
                ElevatedButton(
                    onPressed: () {
                      addCartItem(product.id!);
                      Fluttertoast.showToast(msg: 'Added item successfully');
                    },
                    child: const Text('Add to cart'))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
