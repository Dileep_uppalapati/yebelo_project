import 'package:shared_preferences/shared_preferences.dart';

addCartItem(int id) async {
  final prefs = await SharedPreferences.getInstance();
  getCartData().then((value) {
    if (value == null || value == 0) {
      prefs.setInt('cartItemCount', 1);
    }
    prefs.setInt('cartItemId', id);
  });
}

Future<dynamic> getCartItemData() async {
  final prefs = await SharedPreferences.getInstance();
  int cartItems = prefs.getInt('cartItemId')!;
  return cartItems;
}

Future<dynamic> getCartData() async {
  final prefs = await SharedPreferences.getInstance();
  int cartItems = prefs.getInt('cartItemCount')!;
  return cartItems;
}

clearCart() async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setInt('cartItemCount', 0);
  prefs.setInt('cartItemId', 0);
}
