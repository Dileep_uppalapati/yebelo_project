import 'dart:convert';

import 'package:flutter/services.dart';

Future<dynamic> getLocalJsonData() async {
  final String response = await rootBundle.loadString('assets/data.json');
  final data = await json.decode(response);
  return data;
}

Future<dynamic> getItemJsonData(int id) async {
  var itemData;
  await getLocalJsonData().then((items) {
    for (var item in items) {
      if (id == item['p_id']) {
        itemData = item;
      }
    }
  });
  return itemData;
}
