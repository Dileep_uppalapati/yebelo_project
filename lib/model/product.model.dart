class ProductModel {
  String? name;
  int? id;
  int? cost;
  int? availability;
  String? details;
  String? category;
  String? image;

  ProductModel({
    this.id,
    this.name,
    this.cost,
    this.availability,
    this.details,
    this.category,
    this.image,
  });

  factory ProductModel.fromMap(map) {
    return ProductModel(
      name: map['name'],
      id: map['id'],
      cost: map['cost'],
      availability: map['availability'],
      details: map['details'],
      category: map['category'],
      image: map['image'],
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'id': id,
      'cost': cost,
      'availability': availability,
      'details': details,
      'category': category,
      'image': image,
    };
  }
}
